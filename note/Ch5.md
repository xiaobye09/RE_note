- ## 棧
>一種由高地指向滴地址擴展的資料結構  
>棧內存的作用如下:  
>(1)暫時保存函數內的局部變數  
>(2)紀錄調用函數時傳送的參數  
>(3)保存函數返回後的地址

- ## 棧頂指針(ESP)
>初始狀態下指向棧底