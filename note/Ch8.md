- ## VB專用引擎
>VB文件使用名為MSVBVM60.dll(Microsoft Visual Basic Virtual Machine 6.0)的VB專用引擎(也稱為The Thunder Runtime Engine)。

- ## 本地代碼(N code),偽代碼(P code)
>VB文件可以編譯成為N code 或是 P code,N code 一般使用易於調試器解析的IA-32;P code是一種解釋器(interpreter)語言，他使用由VB引擎實現虛擬機並可以解析的指令(字節碼)。要想解析VB的偽代碼，就需要分析VB引擎並實現模擬器。

>P.S.VB程序採用Windows的事件驅動方式，所以用戶代碼(希望調試的代碼)都在各個事件處理(event handle)程序中，而非main或是WinMain中。


- ## 未文檔化結構體
>VB中使用的各種信息(Dialog,Control,Form,Module,Funtion等)以結構體的形式保存在內部文件，這些文件微軟未正式公布出來(但是網路上有大神解析了)。

- ## 間接調用法(indirect call)
>VC++ VB編譯器中常用的調用法，經過兩次跳轉才會到目標funtion。

- ## RT_MainStruct
>RT_MainStruct結構體的成員是其他結構體的地址。也就是說，VB引擎通過參數傳遞過來的RT_MainStruct結構體獲取程序運行需要的所有訊息。

- ## ThunRTMain
>為VB引擎的代碼，但我們不分析這龐大的引擎，我們要分析的是event handle function內的用戶代碼(比如說按下check鍵後開始比對serial和name)
