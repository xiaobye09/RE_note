- ## IA-32(Intel Architecture)
>* 通用暫存器(General Purpose Registers 32位元 8個)  
>* 段暫存器(Segement Registers 16位元,6個)
>* 程序狀態與控制暫存器(Program Status and Control Register 32位元,1個)
>* 指令指針暫存器(Instruction Pointer 32位元,1個)

<p align="center"><img src="./note/ch4.jpg" /></p><p align="center"><img src="./note/ch4.jpg" /></p>

1. ### 通用暫存器
>用於傳送和暫存數據，也可參與算術邏輯運算並保存運算結果，某些暫存器除了常規用途，還有特被某些彙編指令拿來當作特殊用途。  

>EAX:(針對操作數和結果數據的)累加器  
EBX:(DS段中的數據指針)基指暫存器  
ECX:(字符串和循環操作的)計數器  
EDX:(I/O指針)數據暫存器  
ESI:(字串操作源指針)源變址暫存器  
EDI:(字串操作目標指針)目的變址暫存器  
ESP:(SS段中棧指針)棧指針暫存器  
EBP:(SS段中棧內數據指針)擴展基址指針暫存器  

>此外EAX,ECX也有特殊用途，ECX作為LOOP指令中的計數器，而EAX通常一般用在函數返回值，所有WIN32 API函數都會先把返回值放到EAX中在返回。

>P.S.編寫Window彙編程序時要小心，win32 api函數會在內部使用ECX和EDX,通常都會放重要數據，要呼叫其他函數之前記得先把ECX,EDX存起來。

2. ### 段暫存器
>IA-32的保護模式下，段是一種內存保護技術，它把內存劃分為多個區段，並為每個區段賦予起始地址、範圍；訪問全線等，以<b>保護</b>內存。

<p align="center"><img src="./note/ch4-1.jpg" /></p>

>段內存記錄在SDT(Segment Descriptor Table,段描述符表)中，而段暫存器就持有這個SDT的<b>索引</b>(index)

>每個段寄存器指向段描述符(Segment Descrptor)與虛擬內存結合，形成一個線性地址(Linear address)，借助分頁技術(paging)，線性地址最終被轉換為實際的物理地址(Physical address)

<p align="center"><img src="./note/ch4-2.jpg" /></p>

- ### 程序狀態與控制暫存器
>EFLAGS:Flag register,標誌暫存器,其中有些由系統直接設定，有些則根據程序指令運算結果設置。

<p align="center"><img src="./note/ch4-3.jpg" /></p>

4. ### 指令指針暫存器
>CPU會讀取EIP中一條指令的地址，傳送到指令緩衝區(要等整個完整的指令進入BUFFER才能開始執行)後，EIP暫存器的值就會自動增加，增加的大小為讀取指令的大小。

>與通用暫存器不同，我們沒辦法直接修改EIP的值e.g. mov EIP,1，只能透過其他指令間接修改，包括JMP,Jcc ,CALL,RET，此外我們還可以透過中斷或異常來修改EIP的值。