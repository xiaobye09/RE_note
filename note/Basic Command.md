- ### Restart(Ctrl+F2)
>重新開始調試(終止正在調式的進程後再次運行)  
- ### Step Into(F7)
>執行一句OPcode(操作碼)，若遇到調用命令(call),將進入函數代碼內部。

- ### Step Over(F8)
>執行一句OPcode(操作碼),若遇到調用命令(call),僅執行函數自身，不跟隨進入。

- ### Execute till return(Ctrl+F9)
>一直在函數代碼內部運行,直到遇到RETN命令，跳出函數。

- ### Go to(Ctrl+G)
>移動到指定地址,用來查看代碼或內存，運行時不可用

- ### Execute till Cursor(F4)
>執行到光標位置，即直接跳轉到要調試的位址。

- ### Comment(;)
>添加注釋

- ### User-defined comment
>鼠標右鍵選單Search for User-defined comment

- ### Label(:)
>添加標籤

- ### User-defined label
>鼠標右鍵選單Search for User-defined label

- ### Set/Reset BreakPoint(F2)
>設置或取消斷點(BP)
- ### Run(F9)
>運行(若設置了斷點，則執行至斷點處)

- ### Show the current EIP(*)
>顯示當前EIP(命令指針)位置

- ### Show the previous Cursor(-)
> 顯示上一個光標位置

- ### Preview CALL/JMP address(Enter)
>若光標處有Call/JMP等指令,則跟蹤並顯示相關地址(運行時不可用，簡單查看函數內容時非常有用)

- ### View Breakpoints(Alt + B)
>打開Break points對話框，觀看break points

- ### Search for All referenced text strings
>鼠標右鍵內

- ### edit window(Ctrl+E)
>編輯欲修改數據

- ### Address-Relative to EBP
>在棧窗口，點擊滑鼠右鍵，選擇此功能，可以以EBP為基準點顯示偏移。

- ### Follow in dump
>在代碼窗口或是棧窗口，用滑鼠點擊指定地址，使用右鍵選單中的Follow in dump或Follow in dump->Memory address命令即可。

- ### (硬體斷點)Hardware breakpoint
>右鍵選擇break point->Hardware,on execution,由於在dll或是加殼過後的程式下斷點後，按下Ctrl+F2後會不見，可以用硬體斷點解決這問題，硬體斷點有數量限制

- ### 對某個重複出現的函數設置斷點
>Search for all intermodule calls->選擇要設斷點的函數->右鍵選擇Set breakpoint on every call to xxxx

- ### 將修改過後的data回寫到exe檔中
>copy to executable內的兩個選項都可以使用，依當下需求選擇。

- ### Animate into(Ctrl+F7)
> 反覆執行，Step Into指令(畫面顯示)

- ### Animate over(Ctrl+F8)
> 反覆執行，Step Over指令(畫面顯示)

- ### Trace into(Ctrl+F11)
> 反覆執行，Step Into指令(畫面不顯示)

- ### Trace over(Ctrl+F12)
> 反覆執行，Step Over指令(畫面不顯示)