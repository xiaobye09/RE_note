- ## 棧幀(Stack Frame)
>棧幀就是利用EBP暫存器訪問棧內局部變數、參數、函數返回地址等的手段。  
而EBP則負責行使棧幀指針的職責，透過EBP的值為基準(base)，能夠安全訪問到相關函數的局部變數、參數、返回地址。  
>應用成序採棧幀管理，不論有多少函數調用，棧都能得到比較好的維護。但是由於函數的局部變亮、參數、返回地址等是一次性保存到棧中，利用<b>字串符函數的漏洞(scanf,get等)</b>等，很容易引起棧緩衝區溢出(stack overflow)，最後導致程序或系統崩快(有心人有可以拿來更改返回地址，返回到惡意函數中)

>p.s.最新的編譯器中都帶有一個，優化(optimization)選項，使用後一些簡單的函數將不會行成棧幀。  

<p align="center"><img src="./note/ch7.jpg" /></p>

- 上圖介紹DWORD PTR SS:[EBP-4]的意義。

>函數調用者(Caller)負責清理儲存在棧中的參數，這種方式被稱為cdecl方式;反之，被調用者(Callee)負責清理保存在棧上的參數，這種方式叫做stdcall。這些函數調用規則統稱為調用規約(calling convention)

>最後main函數返回0，使用XOR EAX,EAX將EAX清空成0，比MOV EAX,0還快。
